<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use function Termwind\render;

class ChangeLanguage extends Controller
{
    public function changeLanguage($locale){
        $lang = $locale;
        Session::put('language',$lang);
        return redirect()->back();
    }
}
