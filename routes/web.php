<?php

use App\Http\Controllers\ChangeLanguage;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('welcome/{locale}', [ChangeLanguage::class, 'changeLanguage']);
Route::get('changeLanguage/{locale}', function ($locale) {
    Session::put('language', $locale);
    App::setLocale($locale);
    echo __('message.welcome');
});
Route::get('echo-lang', function () {
    echo __('message.welcome');
});


Route::get('echo-Tuan-anh', function () {
    echo __('message.tuananh');
});
